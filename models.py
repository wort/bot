from aiogram.dispatcher.filters.state import State, StatesGroup


class UserState(StatesGroup):
    idle = State()
    waiting_lesson_selection = State()
    waiting_translation = State()
