import json
import sys

import requests
import telebot
from aiogram import Bot, Dispatcher, types
from aiogram.contrib.fsm_storage.files import JSONStorage
from aiogram.contrib.fsm_storage.redis import RedisStorage
from aiogram.dispatcher import FSMContext
from aiogram.utils import executor
from loguru import logger
from requests.auth import HTTPBasicAuth

from config import get_config, cmds
from models import UserState
from utils import ensure_done

cfg = get_config()

logger.remove(0)
logger.add(sys.stdout, level=cfg.loglevel, serialize=cfg.serialize_logs)

backend_http_base_auth = HTTPBasicAuth(cfg.login, cfg.password)

# Create bot instance
send_bot = telebot.TeleBot(cfg.bot_token)
# Set commands
send_bot.set_my_commands([telebot.types.BotCommand(cmd.command, cmd.description) for cmd in cmds])

logger.info(f"Using fsm type '{cfg.fsm_type}'")
if cfg.fsm_type == 'json':
    storage = JSONStorage("state.json")
elif cfg.fsm_type == 'redis':
    storage = RedisStorage(host=cfg.fsm_host, port=cfg.fsm_port, password=cfg.fsm_password)
else:
    logger.critical("no such fsm storage type")
    exit(-1)

# Create dispatcher for aiogram bot
dp = Dispatcher(bot=Bot(token=cfg.bot_token), storage=storage)


@logger.catch
@dp.message_handler(commands=["start"], state='*')
async def send_welcome(msg: types.Message):
    await UserState.idle.set()
    await ensure_done(msg.answer, args=("First of all, get list of lessons by typing /lessons\n"
                                        "Then /set_lesson\n"
                                        "And start words cycle /get_words",))


@logger.catch
@dp.message_handler(commands=["lessons"], state=UserState.idle)
async def send_lessons(msg: types.Message, state: FSMContext):
    resp = requests.get(url=f"{cfg.base_url}/api/v1/lesson", auth=backend_http_base_auth)
    try:
        rj = resp.json()
        lessons = rj["lessons"]
        lesson_list = '\n'.join([f"{lesson['id']}: {lesson['name']}" for lesson in lessons])

        if 100 >= resp.status_code > 300:
            raise ConnectionError
    except (KeyError, json.decoder.JSONDecodeError, ConnectionError):
        await ensure_done(msg.answer, args=("Couldn't parse request from backend",))
        logger.warning(resp.content)
        return

    user_data = await state.get_data()
    user_data["available_lessons"] = lessons
    await state.set_data(user_data)

    await ensure_done(msg.answer, args=(lesson_list,))


@logger.catch
@dp.message_handler(commands=["set_lesson"], state=UserState.idle)
async def send_lessons(msg: types.Message, state: FSMContext):
    await ensure_done(msg.answer, args=("Please, enter number of lesson...",))
    await UserState.waiting_lesson_selection.set()


@logger.catch
@dp.message_handler(state=UserState.waiting_lesson_selection)
async def save_saved_lesson(msg: types.Message, state: FSMContext):
    user_data = await state.get_data()
    if not user_data.get("available_lessons"):
        await UserState.idle.set()
        await ensure_done(
            msg.answer,
            args=("Please, firstly type in /lessons to get list of available lessons",)
        )
        return

    try:
        selected_lesson_id = int(msg.text)
    except ValueError:
        await ensure_done(msg.answer, args=("Please, enter valid lesson id...",))
        return

    available_lessons = user_data["available_lessons"]

    selected_lesson = None
    for al in available_lessons:
        if al["id"] == selected_lesson_id:
            selected_lesson = al

    if not selected_lesson:
        await UserState.idle.set()
        await ensure_done(msg.answer,
                          args=("There is no such lesson available.\n"
                                "To get list of lessons, please, type /lessons",))
        return

    c_data = await state.get_data()
    c_data["lesson_id"] = selected_lesson_id
    await state.set_data(c_data)
    await UserState.idle.set()
    await ensure_done(msg.answer,
                      args=(f"Successfully set lesson to \"{selected_lesson['name']}\"",))
    await get_words(msg, state)


@logger.catch
@dp.message_handler(commands=["get_words"], state=UserState.idle)
async def get_words(msg: types.Message, state: FSMContext):
    c_data = await state.get_data()
    lsn_id = c_data.get("lesson_id")
    if not lsn_id:
        await UserState.idle.set()
        await ensure_done(msg.answer,
                          args=("No lesson available.\n"
                                "To get list of lessons, please, type /lessons",))
        return

    resp = requests.get(
        url=f"{cfg.base_url}/api/v1/word",
        params=dict(lesson_id=lsn_id, user_id=msg.from_user.id),
        auth=backend_http_base_auth)
    try:
        rj = resp.json()
    except json.decoder.JSONDecodeError:
        await ensure_done(msg.answer, args=("Couldn't parse request from backend",))
        logger.warning(resp.content)

        return

    if rj.get("errors"):
        await ensure_done(msg.answer, args=(rj["errors"],))
        return

    word = rj.get("word")
    if word:
        try:
            c_data["word_id"] = int(word["id"])
            original_word = word["original_word"]
        except (ValueError, KeyError):
            await ensure_done(msg.answer, args=("Couldn't parse request from backend",))
            logger.warning(resp.content)
            return

        await ensure_done(msg.answer, args=(original_word,))
        await state.set_data(c_data)
        await UserState.waiting_translation.set()
    else:
        await ensure_done(msg.answer, args=("Couldn't parse request from backend",))
        logger.warning(resp.content)
        return


@logger.catch
@dp.message_handler(state=UserState.waiting_translation)
async def check_word(msg: types.Message, state: FSMContext):
    c_data = await state.get_data()
    word_id = c_data.get('word_id')
    if not word_id:
        await ensure_done(msg.answer, args=("Please, write /get_words to get a word",))
        return

    resp = requests.get(
        url=f"{cfg.base_url}/api/v1/word/{word_id}",
        params=dict(answer=msg.text),
        auth=backend_http_base_auth
    )
    try:
        rj = resp.json()
        answer_is_right = rj["is_answer_right"]
        right_answer = rj.get("right_answer", "")
    except (KeyError, json.decoder.JSONDecodeError):
        await ensure_done(msg.answer, args=("Couldn't parse request from backend",))
        logger.warning(resp.content)
        return

    if answer_is_right:
        await ensure_done(msg.answer, args=("Yes, answer is right",))
        c_data.update({"retry_on_word": 1})
        await state.set_data(c_data)
        await UserState.idle.set()
        await get_words(msg, state)
    else:
        retries = c_data.get("retry_on_word", 1)
        if retries >= 3:
            await ensure_done(msg.answer, args=(f"Nope, right answer is {right_answer}",))
            c_data.update({"retry_on_word": 1})
            await state.set_data(c_data)
            await UserState.idle.set()
            await get_words(msg, state)
        else:
            c_data.update({"retry_on_word": retries + 1})
            await state.set_data(c_data)
            await ensure_done(msg.answer, args=(f"Try again...",))
            await UserState.waiting_translation.set()


async def shutdown(dispatcher: Dispatcher):
    await dispatcher.storage.close()
    await dispatcher.storage.wait_closed()


@logger.catch
def main() -> None:
    # Start the bot
    logger.info("Starting the bot")
    executor.start_polling(dp, on_shutdown=shutdown)
    logger.info("Bot stopped")


if __name__ == "__main__":
    main()
