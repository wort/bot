FROM python:3.9-alpine

RUN apk update && apk add ca-certificates && apk add build-base && rm -rf /var/cache/apk/*

WORKDIR /app

COPY Pipfile* ./

RUN pip install 'pipenv==2018.11.26' && \
    pipenv install --three --system

COPY . .

ENTRYPOINT ["/bin/sh", "start.sh"]
