import os
from collections import namedtuple
from urllib.parse import urlparse

cmd = namedtuple("Command", ("command", "description"))
cmds = (cmd("start", "Start, mama can"),
        cmd("lessons", "Show lessons"),
        cmd("set_lesson", "Set current lesson"),
        cmd("get_words", "Start words loop"))


class Config:
    def __init__(self, base_url, login, password,
                 bot_token, loglevel, serialize_logs,
                 fsm_type, fsm_host, fsm_port, fsm_password):
        self.base_url = base_url
        self.login = login
        self.password = password
        self.bot_token = bot_token
        self.loglevel = loglevel
        self.serialize_logs = serialize_logs
        self.fsm_type = fsm_type
        self.fsm_host = fsm_host
        self.fsm_port = fsm_port
        self.fsm_password = fsm_password


def get_config() -> Config:
    fsm_type = os.environ.get("FSM_TYPE", "json")
    fsm_host, fsm_port, fsm_password = ("", "", "")
    if fsm_type == 'redis':
        parsed_url = urlparse(os.environ["REDIS_URL"])
        fsm_host = parsed_url.hostname
        fsm_port = parsed_url.port
        fsm_password = parsed_url.password

    return Config(base_url=os.environ["BACKEND_BASE_URL"],
                  login=os.environ["BACKEND_LOGIN"],
                  password=os.environ["BACKEND_PASSWORD"],
                  bot_token=os.environ["BOT_TOKEN"],
                  loglevel=os.environ.get("LOGLEVEL", "debug").upper(),
                  serialize_logs=(os.environ.get("SERIALIZELOGS", "false").lower()
                                  in ["true", "on", "1"]),
                  fsm_type=fsm_type,
                  fsm_host=fsm_host,
                  fsm_port=fsm_port,
                  fsm_password=fsm_password,
                  )
