from time import sleep

from loguru import logger


async def ensure_done(func, args, kwargs=None, *, retries=5, countdown=3):
    if kwargs is None:
        kwargs = {}

    for _ in range(retries):
        try:
            await func(*args, **kwargs)
            break
        except Exception as e:
            logger.warning(e)
            sleep(countdown)
